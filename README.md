# LOGIN

## Task List

- [x] ~~Implement Sign Up page~~
- [x] ~~Implement Forgot Password page~~
- [x] ~~Change hardcoded values for heights/widths to dependent on device size~~ (In progress)
- [x] ~~Clean up Button~~
- [x] ~~Continue as Guest button~~
- [x] ~~Forms need validation / required status - look into replacing current forms with a library~~
  - ~~Possibly rewrite to not use Form pages only have one form page and then use that in each of the screen pages.~~
- [x] ~~Forms styling needs to be consistent~~ (In progress)
- [x] ~~Change Spinner to use more native approach~~
- [x] ~~Clean up some crazy styling~~

## Issues
- Username is slightly to the right when moving the showPassword icon to the correct space.
- Username is too close to Logo now as well and "bottom" on the Logo moves it up
  - It's not respecting "top" and "bottom" (i.e. what I thought was distance away from other elements). Research needed.
- No longer have timer for spinner so spins infinitely on login

### Later tasks

- [ ] Back end communication
- [ ] Clean up spinner to communicate with back end upon sucessful login. Prompt if not.
- [ ] Needs to set some type of variable we can access later to see if they are signed in or not

----

# GenericReactApp

React Native project for a currently generic marketing-focused app.

## Setup

Clone repository and run `npm install` in the directory.

Setup VSCode with the extensions for ESLint and Prettier. ESLint will be checking your code constantly for errors based off of our `.eslintrc.js` config. This config file will change as we start developing. Before committing/often you should also run Prettier, which can be done in VSCode with `F1` and then "Format Document" or manually in CLI with `prettier filename`. You can set it up to run on each save, but for now I won't set it up before commits so please run it yourself.

A good reference for style is the [Airbnb Javascript Style Guide](https://github.com/airbnb/javascript). Our current linter config is based off of a Prettier version of the Airbnb guide, so many of the same recommendations apply.

To start the web interface for the app, run either `npm run web` or `yarn web` if yarn is installed globally.

Remember to branch when developing new features and testing. Do **NOT** commit to master without code review. [Guide to Git](https://guides.github.com/introduction/git-handbook/). If you don't understand why I recommend certain things, [General Project Guidelines](https://github.com/elsewhencode/project-guidelines).

## Available Scripts

### `npm start`

Runs your app in development mode.

Open it in the [Expo app](https://expo.io) on your phone to view it. It will reload if you save edits to your files, and you will see build errors and logs in the terminal.

Sometimes you may need to reset or clear the React Native packager's cache. To do so, you can pass the `--reset-cache` flag to the start script:

```
npm start -- --reset-cache
# or
yarn start -- --reset-cache
```

#### `npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup). We also recommend installing Genymotion as your Android emulator. Once you've finished setting up the native build environment, there are two options for making the right copy of `adb` available to Create React Native App:

#### `npm run web`

Like `npm start`, but attempts to open your app to be browsable on the web using webpack-dev-server. This will be the main command for testing the app at least initially since it does not require any emulators or Expo.

##### Using Android Studio's `adb`

1. Make sure that you can run adb from your terminal.
2. Open Genymotion and navigate to `Settings -> ADB`. Select “Use custom Android SDK tools” and update with your [Android SDK directory](https://stackoverflow.com/questions/25176594/android-sdk-location).

##### Using Genymotion's `adb`

1. Find Genymotion’s copy of adb. On macOS for example, this is normally `/Applications/Genymotion.app/Contents/MacOS/tools/`.
2. Add the Genymotion tools directory to your path (instructions for [Mac](http://osxdaily.com/2014/08/14/add-new-path-to-path-command-line/), [Linux](http://www.computerhope.com/issues/ch001647.htm), and [Windows](https://www.howtogeek.com/118594/how-to-edit-your-system-path-for-easy-command-line-access/)).
3. Make sure that you can run adb from your terminal.

#### `npm run eject`

This will start the process of "ejecting" from Create React Native App's build scripts. You'll be asked a couple of questions about how you'd like to build your project.

**Warning:** Running eject is a permanent action (aside from whatever version control system you use). An ejected app will require you to have an [Xcode and/or Android Studio environment](https://facebook.github.io/react-native/docs/getting-started.html) set up.

## Writing and Running Tests

This project is set up to use [jest](https://facebook.github.io/jest/) for tests. You can configure whatever testing strategy you like, but jest works out of the box. Create test files in directories called `__tests__` or with the `.test` extension to have the files loaded by jest. See the [the template project](https://github.com/react-community/create-react-native-app/blob/master/react-native-scripts/template/App.test.js) for an example test. The [jest documentation](https://facebook.github.io/jest/docs/en/getting-started.html) is also a wonderful resource, as is the [React Native testing tutorial](https://facebook.github.io/jest/docs/en/tutorial-react-native.html).