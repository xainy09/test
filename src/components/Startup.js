import React from "react";
import { Router, Scene } from "react-native-router-flux";

import LoginScreen from "./login/LoginScreen";
import SecondScreen from "./login/SecondScreen";
import SignUpScreen from "./login/SignUpScreen";
import ForgotPassScreen from "./login/ForgotPassScreen";

const Startup = () => (
  // uses react-native-router-flux to create a Router/Scene that
  // defines the routes

  // Dan is using import { TabNavigator, TabBarBottom } from 'react-navigation';
  // StackNavigator for Tarun

  <Router>
    <Scene key="root">
      <Scene key="loginScreen" component={LoginScreen} animation="fade" />
      <Scene key="signUpScreen" component={SignUpScreen} animation="fade" />
      <Scene key="secondScreen" component={SecondScreen} animation="fade" />
      <Scene
        key="forgotPassScreen"
        component={ForgotPassScreen}
        animation="fade"
      />
    </Scene>
  </Router>
);

export default Startup;
