import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Actions } from "react-native-router-flux";
import { Button } from "react-native-elements";

const styles = StyleSheet.create({
  buttonViewStyle: {
    top: 20,
    marginTop: 30,
    padding: 20
  }
});

export default class LoginForm extends Component {
  render() {
    return (
      <View style={styles.buttonViewStyle}>
        <Button
          text="Remind me Later"
          onPress={() => Actions.secondScreen()}
          buttonStyle={{ width: Dimensions.get("screen").width / 5 }}
        />
      </View>
    );
  }
}
