import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert
} from "react-native";
import { Google } from "expo";

// this is terrible - has Alert despite being Web based, just doesn't work on Web at all,
// uses someone elses ClientIDs (and doesn't have any for StandAlone)
// Remove this button if it doesn't work at all - I am not pushing broken features

export default class GoogleSignin extends Component {
  _handleGoogleLogin = async () => {
    try {
      const { type, user } = await Google.logInAsync({
        androidStandaloneAppClientId: "<ANDROID_CLIENT_ID>",
        iosStandaloneAppClientId: "<IOS_CLIENT_ID>",
        androidClientId:
          "603386649315-9rbv8vmv2vvftetfbvlrbufcps1fajqf.apps.googleusercontent.com",
        iosClientId:
          "603386649315-vp4revvrcgrcjme51ebuhbkbspl048l9.apps.googleusercontent.com",
        scopes: ["profile", "email"]
      });

      switch (type) {
        case "success": {
          Alert.alert("Logged in!", `Hi ${user.name}!`);
          break;
        }
        case "cancel": {
          Alert.alert("Cancelled!", "Login was cancelled!");
          break;
        }
        default: {
          Alert.alert("Oops!", "Login failed!");
        }
      }
    } catch (e) {
      Alert.alert("Oops!", "Login failed!");
    }
  };

  //will change the image after this works with graphql.

  render() {
    return (
      <TouchableOpacity onPress={this._handleGoogleLogin}>
        <Image
          style={styles.button}
          source={require("./images/google_signin.png")}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    top: 15,
    padding: 10,
    width: Dimensions.get("screen").width / 6,
    resizeMode: "cover"
  }
});
