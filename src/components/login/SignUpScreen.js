import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import Logo from "./Logo";
import NewButtonSubmit from "./NewButtonSubmit";
import FormClass from "./FormClass";

export default class SignUpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      formErrorMsg: "",
      // these do not need to be explicitly defined since onTextChange will make them
      // however, defining so that later on "Submit" you could see if still empty
      // they are [txt, errorMsg]
      firstName: "",
      lastName: "",
      email: "",
      username: "",
      password: ""
    };
    this.onTextChange = this.onTextChange.bind(this);
    this.setFormError = this.setFormError.bind(this);
    this.showPass = this.showPass.bind(this);
  }

  // Passed down as a prop to get text change of all forms
  onTextChange(txtWithErr, formType) {
    this.setState(() => {
      return { [formType]: txtWithErr };
    });
  }

  // Passed down as a prop to show form error
  setFormError(txt) {
    this.setState(() => {
      return { formErrorMsg: txt };
    });
  }

  // Passed down as a prop for use in the showPass eye button
  showPass() {
    if (this.state.press === false) {
      this.setState({ showPass: false, press: true });
    } else {
      this.setState({ showPass: true, press: false });
    }
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#e5ffff",
        paddingLeft: 60
      }
    });

    return (
      <View style={styles.container}>
        <View style={{ bottom: 30 }}>
          <Logo />
        </View>
        <FormClass formType="firstName" onTextChange={this.onTextChange} />
        <FormClass formType="lastName" onTextChange={this.onTextChange} />
        <FormClass formType="email" onTextChange={this.onTextChange} />
        <FormClass formType="username" onTextChange={this.onTextChange} />
        <FormClass
          formType="password"
          onTextChange={this.onTextChange}
          shouldShowPass={this.state.showPass}
        />
        <Text style={{ textAlign: "center" }}> {this.state.formErrorMsg}</Text>
        <NewButtonSubmit
          setFormError={this.setFormError}
          text="SIGNUP"
          {...this.state}
        />
      </View>
    );
  }
}
