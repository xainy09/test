import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";
import Logo from "./Logo";
import FormClass from "./FormClass";
import NewButtonSubmit from "./NewButtonSubmit";
import SignUpText from "./SignUpText";
import Guest from "./Guest";
import GoogleSignIn from "./GoogleSignIn";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#3498db",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  loginShowPass: {
    flexDirection: "row"
  }
});

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      formErrorMsg: "",
      // these do not need to be explicitly
      // defined since onTextChange will make them
      username: "",
      password: "",
      firstName: "",
      lastName: "",
      email: ""
    };

    /*
      Adding a Dimensions event handler to detect a window size change
      Would also work for orientation change.
      The other method for this would be to use props update for all components,
      but I was not able to get it to work.

      Also, I'd add this in Startup.js but getting "updating unmounted component" errors
      It would be important later to remove the event listener on un-mount
      of the LoginScreen.
    */

    Dimensions.addEventListener("change", () => {
      this.forceUpdate();
    });

    this.showPass = this.showPass.bind(this);
    this.onTextChange = this.onTextChange.bind(this);
    this.setFormError = this.setFormError.bind(this);
  }

  // Passed down as a prop to get text change of all forms
  onTextChange(txt, formType) {
    this.setState(() => {
      return { [formType]: txt };
    });
  }

  // Passed down as a prop to show form error
  setFormError(txt) {
    this.setState(() => {
      return { formErrorMsg: txt };
    });
  }

  // Passed down as a prop for use in the showPass eye button
  showPass() {
    if (this.state.press === false) {
      this.setState({ showPass: false, press: true });
    } else {
      this.setState({ showPass: true, press: false });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <GoogleSignIn />
        <View style={{ bottom: 10 }}>
          <Logo />
        </View>
        <FormClass formType="username" onTextChange={this.onTextChange} />

        {/* Putting password and showpass in same view */}
        <View style={styles.loginShowPass}>
          <FormClass
            formType="password"
            onTextChange={this.onTextChange}
            shouldShowPass={this.state.showPass}
          />
          {/* This works, but issue with centering. 
          <FormClass formType="eyePass" showPass={this.showPass} /> */}
        </View>

        <NewButtonSubmit
          setFormError={this.setFormError}
          text="LOGIN"
          {...this.state}
        />
        <Guest />
        <SignUpText />
        <Text> {this.state.formErrorMsg} </Text>
      </View>
    );
  }
}
