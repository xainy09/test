import React, { Component } from "react";
import { ActivityIndicator, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-elements";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  // the buttonStyle specific styles
  // there is also a regular style Button imports from
  // TouchableNativeFeedback/TouchableOpacity props
  btnStyle: {
    backgroundColor: "green",
    top: 50,
    width: Dimensions.get("window").width / 4.3
  },
  spinnerStyle: {
    top: 50
  }
});

export default class NewButtonSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSpinner: false,
      showButton: true
    };
  }

  // Set error and pass it one layer above
  setError(txt) {
    this.props.setFormError(txt);
  }

  // arrow function to not have to bind `this`
  validateAndGet = () => {
    // May not need this first check later, only need if
    // this.props.item[1] doesn't exist (when it's not array and is "")
    // Also looking at props.text here is hacky

    console.log(this.props);

    if (this.props.text.toLowerCase() === "signup") {
      if (
        this.props.password === "" ||
        this.props.username === "" ||
        this.props.firstName === "" ||
        this.props.lastName === "" ||
        this.props.email === ""
      ) {
        this.setError("Form not valid");
        return;
      } else if (
        this.props.password[1] ||
        this.props.username[1] ||
        this.props.firstName[1] ||
        this.props.lastName[1] ||
        this.props.email[1]
      ) {
        this.setError("Form not valid 2");
        return;
      }
    } else if (this.props.text.toLowerCase() === "login") {
      if (this.props.password === "" || this.props.username === "") {
        this.setError("Form not valid");
        return;
      } else if (this.props.password[1] || this.props.username[1]) {
        this.setError("Form not valid 2");
        return;
      }
      // Problem with the hack that I mentioned - "SUBMIT" is the button text
      // for the forgot password form, which only has email
    } else if (this.props.text.toLowerCase() === "submit") {
      if (this.props.email === "") {
        this.setError("Form not valid");
        return;
      } else if (this.props.email[1]) {
        this.setError("Form not valid 2");
        return;
      }
    }

    // set back to no error if it got here
    this.setError("");

    // change to spinner
    this.setState({ showSpinner: true });

    // query
    // Eventually change page with Actions.secondScreen() / any router
  };

  render() {
    if (this.state.showSpinner) {
      if (this.state.showButton) {
        // This is bad. Updating state during a render gives a warning.
        // Will only happen on one render but should be avoided.
        this.setState({ showButton: false });
      }

      return (
        <ActivityIndicator
          size="large"
          color="#0000ff"
          style={styles.spinnerStyle}
        />
      );

      // this eventually needs a way to stop spinning if successful login,
      // or re-show button on not successful
    } else {
      return (
        <Button
          buttonStyle={styles.btnStyle}
          text={this.props.text}
          onPress={this.validateAndGet}
          visible={this.state.showButton}
        />
      );
    }
  }
}

NewButtonSubmit.propTypes = {
  text: PropTypes.string.isRequired,
  username: PropTypes.any.isRequired, // ugh I am using "any" too often
  // currently can be either array or blank string
  password: PropTypes.any.isRequired,
  lastName: PropTypes.any.isRequired,
  firstName: PropTypes.any.isRequired,
  email: PropTypes.any.isRequired,
  setFormError: PropTypes.func.isRequired
};

NewButtonSubmit.defaultProps = {
  password: "",
  lastName: "",
  firstName: "",
  email: "",
  username: ""
};
