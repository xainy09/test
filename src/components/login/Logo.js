import React, { Component } from "react";
import { StyleSheet, View, Image, Dimensions } from "react-native";

const logo = require("./images/logo.jpg");

export default class Logo extends Component {
  render() {
    const styles = StyleSheet.create({
      logo: {
        top: 30,
        bottom: 30,
        marginBottom: 30,
        height: Dimensions.get("screen").height / 8,
        width: Dimensions.get("screen").height / 8 // I want it to be a square
      }
    });

    return <Image style={styles.logo} source={logo} />;
  }
}
