import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import Logo from "./Logo";
import NewButtonSubmit from "./NewButtonSubmit";
import FormClass from "./FormClass";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#cfff95",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    textAlign: "center"
  }
});

export default class ForgotPassScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formErrorMsg: "",
      email: ""
    };
    this.onTextChange = this.onTextChange.bind(this);
    this.setFormError = this.setFormError.bind(this);
  }

  // Passed down as a prop to get text change of all forms
  onTextChange(txt, formType) {
    this.setState(() => {
      return { [formType]: txt };
    });
  }

  // Passed down as a prop to show form error
  setFormError(txt) {
    this.setState(() => {
      return { formErrorMsg: txt };
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Logo />
        <Text style={styles.header}>
          Enter your Email address to reset your Password
        </Text>
        <FormClass formType="email" onTextChange={this.onTextChange} />
        <Text> {this.state.formErrorMsg} </Text>
        <NewButtonSubmit
          setFormError={this.setFormError}
          text="SUBMIT"
          {...this.state}
        />
      </View>
    );
  }
}
