import React, { Component } from "react";
import PropTypes from "prop-types";
import { Input } from "react-native-elements";
/* eslint-disable no-useless-escape */

/* Define Regexes for validation */

const validateEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const validateRealName = name => {
  const re = /^[a-zA-Z'-]+$/;
  return re.test(String(name));
};

const validateUsername = username => {
  // could be brought into the regex instead of separate
  if (username.length < 3) {
    return false;
  }
  // don't allow characters other than letters/numbers for first letter
  const re = /^[a-zA-Z0-9]+([-_\.][a-zA-Z0-9]+)*[a-zA-Z0-9]$/;
  return re.test(String(username).toLowerCase());
};

const validatePassword = password => {
  if (password.length < 6) {
    return false;
  }
  // Require at least one letter and one number
  const re = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
  return re.test(String(password));
};

export default class UserInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMsg: null
    };
  }

  changeText(txt) {
    // empty string - don't show error
    if (txt.length === 0) {
      this.setState({ errorMsg: null });
      return;
    }

    // Validation
    const errMsg = (function validate(funcType) {
      switch (funcType) {
        case "email":
          return validateEmail(txt) ? null : "Email not valid";
        case "name":
          return validateRealName(txt) ? null : "Name not valid";
        case "username":
          return validateUsername(txt) ? null : "Username not valid";
        case "password":
          return validatePassword(txt)
            ? null
            : "Password must be at least 6 characters, one char of both a number and letter";
        default:
          throw new Error("Validation type not valid");
      }
    })(this.props.type);

    this.setState({ errorMsg: errMsg });

    // Raise the state
    // Pass both text and the errMsg text
    this.props.onTextChange([txt, errMsg]);
  }

  render() {
    return (
      <Input
        onChangeText={txt => this.changeText(txt)}
        errorMessage={this.state.errorMsg}
        errorStyle={{ color: "red" }}
        placeholder={this.props.placeholder}
        autoCapitalize={this.props.autoCapitalize}
        autoCorrect={this.props.autoCorrect}
        onFocus={this.props.onFocus}
        keyboardType={this.props.keyboardType}
        autoFocus={this.props.autoFocus}
        secureTextEntry={this.props.secureTextEntry}
        // Idk why this works with the "unnecessary ternary" but throws errors without it,
        // what evaluates to true in JS is confusing
        displayError={this.state.errorMsg ? true : false}
        style={this.props.style}
        containerStyle={this.props.containerStyle}
      />
    );
  }
}

UserInput.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  autoCapitalize: PropTypes.string.isRequired,
  autoCorrect: PropTypes.bool.isRequired,
  onFocus: PropTypes.string,
  keyboardType: PropTypes.string.isRequired,
  autoFocus: PropTypes.bool.isRequired,
  secureTextEntry: PropTypes.bool,
  style: PropTypes.any, // usually would be Text.PropTypes.style, not in react native anymore
  containerStyle: PropTypes.any, // same as above
  onTextChange: PropTypes.func.isRequired
};

// All the props that may not need to be specified
UserInput.defaultProps = {
  onFocus: null,
  style: {},
  type: "",
  secureTextEntry: false,
  containerStyle: {}
};
