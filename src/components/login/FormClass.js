import React, { Component } from "react";
import { TouchableOpacity, StyleSheet, Image, Dimensions } from "react-native";
import PropTypes from "prop-types";
import UserInput from "./UserInput";
import eyeImg from "./images/eye_black.png";
/* eslint react/require-default-props: 0 */

const styles = StyleSheet.create({
  /* Idea to properly style the button eye:
    Make View of password larger than normal and put the button eye to the right.
    Right now, the implementation is absolute and just terrible
  */
  btnEye: {},
  iconEye: {
    width: 25,
    height: 25,
    tintColor: "rgba(0,0,0,0.2)"
  }
});

export default class FormClass extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onTextChange = this.onTextChange.bind(this);
  }

  onTextChange(txtWithError) {
    this.props.onTextChange(txtWithError, this.props.formType);
  }

  render() {
    // Define different form types here

    if (this.props.formType === "firstName") {
      return (
        <UserInput
          secureTextEntry={false}
          placeholder="First Name"
          type="name"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="default"
          autoFocus
          onTextChange={this.onTextChange} // to raise the state
        />
      );
    }

    if (this.props.formType === "lastName") {
      return (
        <UserInput
          secureTextEntry={false}
          placeholder="Last Name"
          type="name"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="default"
          autoFocus={false}
          onTextChange={this.onTextChange}
        />
      );
    }

    if (this.props.formType === "username") {
      return (
        <UserInput
          secureTextEntry={false}
          placeholder="Username"
          autoCapitalize="none"
          type="username"
          autoCorrect={false}
          keyboardType="default"
          autoFocus={false}
          onTextChange={this.onTextChange}
          containerStyle={{}}
        />
      );
    }

    if (this.props.formType === "password") {
      return (
        <UserInput
          secureTextEntry={this.props.shouldShowPass}
          placeholder="Password"
          autoCapitalize="none"
          type="password"
          autoCorrect={false}
          keyboardType="default"
          autoFocus={false}
          onTextChange={this.onTextChange}
          containerStyle={{}}
        />
      );
    }

    if (this.props.formType === "eyePass") {
      return (
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.btnEye}
          onPress={this.props.showPass}
        >
          <Image source={eyeImg} style={styles.iconEye} />
        </TouchableOpacity>
      );
    }

    if (this.props.formType === "email") {
      return (
        <UserInput
          secureTextEntry={false}
          placeholder="Email Address"
          autoCapitalize="none"
          type="email"
          autoCorrect={false}
          keyboardType="email-address"
          autoFocus={false}
          onTextChange={this.onTextChange}
        />
      );
    }

    return "Will never get here";
  }
}

FormClass.propTypes = {
  formType: PropTypes.string.isRequired,
  showPass: PropTypes.func,
  shouldShowPass: PropTypes.bool,
  onTextChange: PropTypes.func
};
