import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";

const styles = StyleSheet.create({
  container: {
    top: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    paddingBottom: 20
  },
  text: {
    color: "red",
    backgroundColor: "transparent",
    paddingLeft: 20,
    fontSize: 18
  }
});

export default class SignUpText extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* Using TouchableOpacity for pressing Text */}
        <TouchableOpacity onPress={() => Actions.signUpScreen()}>
          <Text style={styles.text}>Create Account</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Actions.forgotPassScreen()}>
          <Text style={styles.text}>Forgot Password?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
