// IMPORTANT THIS POLYFILL MUST BE INCLUDED BEFORE any RNW (react-native) imports
import 'resize-observer-polyfill/dist/ResizeObserver.global'
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
