module.exports = {
    // airbnb config that turns off rules that prettier doesn't agree with (prettier-favored)
    "extends": [ 
        "airbnb",
        "prettier",
        "prettier/react"
    ],
    "plugins": [
        "react",
        "react-native"
      ],
      "rules": {
        "react/jsx-filename-extension": [2, { "extensions": [".js", ".jsx"] }],
        "no-unused-vars": 1,
        "react/destructuring-assignment": 0,
        // disabling pure function checks as code needs to have a render function
        // for updating Dimensions but will show error in eslint
        "react/prefer-stateless-function": 0
      }
};